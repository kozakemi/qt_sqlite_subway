#ifndef SQLITEOPERATE_H
#define SQLITEOPERATE_H
#include <QSqlDatabase>
#include <QSqlError>
#include <QSqlQuery>
#include <QDebug>
#include <QSqlRecord>
#include <QFileInfo>



class SqliteOperate
{
private:

    QSqlDatabase database;
    void rm_all_file(QString filePath);

    QList<QString> sqlite_select_cmd(QString select_sql);
    int sqlite_commit_cmd(QString commit_sql);
    void file_insert(QString filename,QString data);//file 行插入 LineQD.txt
    QList<QString> select_linestations(QString Line_id);//递归查询

    QList<QString> select_all_lines();
    QList<QString> select_all_satations();
    int select_station_in_line(QString Station_id,QString Line_id);
    QString generateRandomConnectionName();
    void open_database();
    void close_database();
public:
    SqliteOperate();
    ~SqliteOperate();
    void select_all_data_txt();//查询所有信息,按照格式写入
    int insert_line(QString line_name,QString line_color);
    int insert_station(QString station_name, QString latitude,QString longitude);
//    int insert_station_and_line(QString station_name, QString latitude,QString longitude,QString Line_name,QString previous_station_name,QString next_station_name);
    int insert_station_to_line(QString station_name,QString Line_name,QString previous_station_name,QString next_station_name);
};

#endif // SQLITEOPERATE_H
