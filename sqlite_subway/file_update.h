#ifndef FILE_UPDATE_H
#define FILE_UPDATE_H
#include "sqliteoperate.h"
#include "downloadtool.h"
#include <QFile>
#include <QTextStream>
#include <QTimer>
#include <QMessageBox>
int verision_file_inspect();
int file_update();
int file_update_MessageBox(int value);
QString search_verision();

//void test_sql();
#endif // FILE_UPDATE_H
