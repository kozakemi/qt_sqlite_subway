#include "file_update.h"
const QString version_file_url="https://gitlab.com/kozakemi/subway_sqlite_python/-/raw/master/version\?ref_type\=heads\&inline\=false";
const QString sqlfile_url="https://gitlab.com/kozakemi/subway_sqlite_python/-/raw/master/subway_qd.sqlite3?ref_type=heads&inline=false";
int verision_file_inspect() {
    QFile file("version");
    if (!file.open(QIODevice::ReadOnly | QIODevice::Text)) {
        return -1;
    } else {
        QTextStream in(&file);
        QString version = in.readLine(); // 假设版本号在文件的第一行
        DownloadTool* dT;
        dT = new DownloadTool(version_file_url, QApplication::applicationDirPath() + "/oss");
        dT->startDownload();
        QEventLoop loop;
        int exitCode = -1; // 用于存储退出代码
        QObject::connect(dT, &DownloadTool::sigDownloadFinished, [&]() {
            qDebug() << "下载完成";
            QFile file_oss("oss/version");
            if (!file_oss.open(QIODevice::ReadOnly | QIODevice::Text)) {
                exitCode = -1;
            } else {
                QTextStream in(&file_oss);
                QString version_oss = in.readLine(); // 假设版本号在文件的第一行
                qDebug() << "文件版本111：" << version_oss;
                file_oss.close();
                if (version_oss > version) {
                    exitCode = 1;
                    qDebug() << "远程更新";
                } else {
                    qDebug() << "与远程一致或更新";
                    exitCode = 0;
                }
            }
            loop.quit(); // 退出事件循环
        });

        loop.exec();  // 进入事件循环，等待信号触发

        delete dT;  // 需要手动释放资源，避免内存泄漏
        return exitCode;  // 返回退出代码作为结果
    }
}

QString search_verision(){
    QFile file("version");
    if (!file.open(QIODevice::ReadOnly | QIODevice::Text)) {
        return "-1";
    } else {
        QTextStream in(&file);
        QString version = in.readLine(); // 假设版本号在文件的第一行
        return  version;
    }
}

int file_update()
{
    DownloadTool* dT;
    dT = new DownloadTool(version_file_url, QApplication::applicationDirPath() + "/");
    dT->startDownload();
    QEventLoop loop;
    QObject::connect(dT, &DownloadTool::sigDownloadFinished, [&]() {
        qDebug()<<"";
        loop.quit(); // 退出事件循环
    });
    loop.exec();  // 进入事件循环，等待信号触发
    DownloadTool* dT1;
    dT1 = new DownloadTool(sqlfile_url,QApplication::applicationDirPath()+"/");
    dT1->startDownload();
    QEventLoop loop1;
    QObject::connect(dT1, &DownloadTool::sigDownloadFinished, [&]() {
        qDebug()<<"";
        loop1.quit(); // 退出事件循环
    });
    loop1.exec();  // 进入事件循环，等待信号触发
    delete dT;
    delete dT1;
    return 0;
}

int file_update_MessageBox(int value){
    int file_flage=verision_file_inspect();
    if(file_flage==1){
        if(value){
            QMessageBox msgBox;
            msgBox.setWindowTitle("提示");
            msgBox.setText("有可用的软件更新，是否执行更新操作？");
            msgBox.setIcon(QMessageBox::Question);
            msgBox.setStandardButtons(QMessageBox::Ok | QMessageBox::Cancel);
            msgBox.setDefaultButton(QMessageBox::Ok);
            int ret = msgBox.exec();
            // 判断用户点击的按钮
            if (ret == QMessageBox::Ok) {
                file_update();
            } else {

            }
        }
    }else if(file_flage==0){
        if(value){
            QMessageBox msgBox;
            msgBox.setText("已是最新版本");
            msgBox.exec();
        }
    }else{
        QMessageBox msgBox;
        msgBox.setWindowTitle("提示");
        msgBox.setText("资源文件缺失,是否重新下载");
        msgBox.setIcon(QMessageBox::Question);
        msgBox.setStandardButtons(QMessageBox::Ok | QMessageBox::Cancel);
        msgBox.setDefaultButton(QMessageBox::Ok);
        int ret = msgBox.exec();
        // 判断用户点击的按钮
        if (ret == QMessageBox::Ok) {
            if(file_update()==0){
                QMessageBox msgBox;
                msgBox.setText("下载完毕");
            }
        } else {

        }
    }
    return file_flage;

}
//void test_sql(){
//    SqliteOperate sqliteOperate1;
//    qDebug()<<"4:"<<sqliteOperate1.insert_station("陈路","36.2","120.4");
//    qDebug()<<"5:"<<sqliteOperate1.insert_station_to_line("马路","8号线","大洋","青岛北站");
//    qDebug()<<"1:"<<sqliteOperate1.insert_line("10号线","#00621");
//    sqliteOperate1.select_all_data_txt();
//}
