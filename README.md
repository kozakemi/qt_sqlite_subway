# qt_sqlite_subway
### 依赖
windows推荐使用 Min GW -64bit，并且安装openssl 1.1版本，
openssl下载：http://slproweb.com/products/Win32OpenSSL.html https://blog.csdn.net/zyhse/article/details/108186278，
检查openssl版本 `openssl version` ,如果找不到命令，
则需要添加环境变量 `C:\Program Files\OpenSSL-Win64\bin`
Linux 安装openssl 1.1
为何是1.1？
检查支持的openssl版本（本地与依赖不需要严格的完全一致）
``` c++
qDebug()<<"QSslSocket="<<QSslSocket::sslLibraryBuildVersionString();

```
使用以下命令检查,应该包含HTTPS
``` c++
qDebug() << "OpenSSL支持情况:" << QSslSocket::supportsSsl();

```
### 介绍
这是一个用于Qt青岛地铁项目的sqlite部分的项目代码,主要是sqlit的命令及实现

### 疑问？
请提交Issue

### 感谢
感谢组员的支持与配合

### 项目关联
[QDsubway](https://gitee.com/alibaba12/test)

[subway_sqlite_python](https://gitlab.com/kozakemi/subway_sqlite_python)
